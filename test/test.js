const assert = require('assert');
const resta = require('../src/app');
const chai = require('chai');
const expect = chai.expect;

describe('Resta', function() {
  it('debe ser = 3 para 5 - 2', function() {
    assert.strictEqual(resta(5, 2), 3);
  });

  it('debe ser = -1 para 2 - 3', function() {
    assert.strictEqual(resta(2, 3), -1);
  });
  it('debe ser = 0 para  2 - 2', function() {
    assert.strictEqual(resta(2, 2), 0);
  });
});

describe('Función resta', () => {
  it('debería ser = 1.5', () => {
    expect(resta(2.5, 1)).to.equal(1.5);
  });

  it('debería ser = -50', () => {
    expect(resta(50, 100)).to.equal(-50);
  });
});


